<?php

namespace App\Http\Controllers;

use App\User;
use App\video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo $videos=video::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "<h3>CREATE</h3>";
        echo '<br><form action="/video" method="POST">';
        echo '<br><input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<br><input type="text" name="name">';
        echo '<br><input type="number" name="views">';
        echo '<br><input type="submit">';
        echo '<br></form >';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        video::create($request->all());
        return redirect()->route('video.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(video $video)
    {
        echo "<h3>SHOW</h3>";
        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>Name</th>";
        echo "<th>Views</th>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>$video->name</td>";
        echo "<td>$video->views</td>";
        echo "</tr>";
        echo "</table>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(video $video)
    {
        //
        echo "<h3>EDIT: $video->name</h3>";
        echo "<form action='/video/$video->id' method='POST'>";
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="hidden" name="_method" value="PUT">';
        echo "<input type='text' name='name' value='$video->name'>";
        echo "<input type='number' name='view' value='$video->views'>";
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(video $video,Request $request)
    {
        //
        $video->update($request->all());
        return redirect()->route('video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
