<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ManyController extends Controller
{
    //
    public function Manytomany(){
        $products=[
            [
                'name' => 'HP Omen',
                'price' => '81900',
                'description' => 'HP Omen Core i7 9th Gen - (8 GB/1 TB HDD/256 GB SSD/Windows 10 Home/4 GB Graphics/NVIDIA Geforce GTX 1650) 15-dc1093TX Gaming Laptop  (15.6 inch, Shadow Black, 2.38 kg)'
            ],
            [
                'name' => 'Apple MacBook Pro ',
                'price' => '224990',
                'description' => 'Apple MacBook Pro Core i9 8th Gen - (16 GB/512 GB SSD/Mac OS Mojave/4 GB Graphics) MV932HN  (15.4 inch, Silver, 1.83 kg)'
            ]
        ];
//    $categories=
//        [
//            [
//                'name'=> 'Gamming Laptop',
//                'description'=>'Its gaming laptop'
//
//            ],
//            [
//                'name'=> 'Professional Laptop',
//                'description'=>'Its Professional laptops'
//
//            ],
//        ];
    //---
//        $category=Category::create($categories[1]);
        $category= Category::all();
//        $products=Product::find([1,2]);
//        $category->products()->attach($products);
        //---
//        $product=Product::Create($products[0]);
        $product= Product::all();
//        $categories=Category::find([1,2]);
//        $product->categories()->attach($categories);

        $category= Category::find(1);
        $category->products;

        $product=Product::find(1);
        $product->categories;

//        $category = Category::find(2);
//        $product = Product::find(1);
//        $product->categories()->detach($category);
//
//        $cat = Category::find(1);
//        $cat->products()->create($products[1]);
//        return "ok";
    }
    public function category($id)
    {
        $category = Category::with('products')->find($id);
        return $category;
    }

    public function product($id)
    {
        $product = Product::with('categories')->find($id);
        return Product::with('categories')->find($id);
    }

}
