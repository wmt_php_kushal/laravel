<?php

namespace App\Http\Controllers;

//use App\User;
use App\User;
use App\video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        //
//        dd('Index');
        $u=$user::all();
        return view('display',compact('u'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        //
        return view('createform',compact('user'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create($request->all());
        return redirect()->route('user.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo User::find($id);
        //
    }
//    public function show(User $user)
//    {
//        //
//        echo $user::all();
//        //
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        //
        return view('updateform',compact('user'));

//        echo '<form action="/user/'.$user->id.'" method="POST">';
    //        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
    //        echo '<input type="hidden" name="_method" value="PUT" >';
    //        echo '<br>Name:<input type="name" name="name" value="'.$user->name.'"  >';
    //        echo '<br>Email:<input type="email" name="email" value="'.$user->email.'" >';
    //        echo '<br>Password:<input type="password" name="password" value="'.$user->password.'" >';
    //        echo '<input type="submit">';
//        echo '</form>';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user,Request $request)
    {
        //
//        dd('Update');
        $user->update($request->all());
        return redirect()->route('user.index');
//
//        $user1= User::find($id);
//        $user1->email=$request->email;
//        $user1->password = bcrypt($request->password);
//        $user1->save();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $user->delete();
        return redirect()->route('user.index');
        //
    }
}
