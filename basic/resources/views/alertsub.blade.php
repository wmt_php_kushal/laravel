{{--@component('alert')--}}
{{--    @slot('title')--}}
{{--        Forbidden--}}
{{--    @endslot--}}

{{--    You are not allowed to access this resource!--}}
{{--@endcomponent--}}

@componentfirst(['custom.alert', 'alert'])

  @component('alert')
      @slot('class')
          danger
      @endslot
      @slot('title')
          danger message
      @endslot
      This is a danger message, just for testing purpose.
  @endcomponent
  @component('alert')
      @slot('class')
          danger2
      @endslot
      @slot('title')
          danger message2
              <div class="container">
                  Hello, {{ $name }}.
              </div>
      @endslot
      This is a danger message, just for testing purpose.


  @endcomponent
  @slot('class')
      <strong>Whoops!</strong> Something went wrong!
  @endslot
  @slot('title')  <strong>Whoops!</strong> Something went wrong!
  @endslot
  @endcomponentfirst
