<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use \App\User;

Route::get('form', function () {

    echo '<form action="form" method="post">';
//    echo '<form action="form" method="get">';
    echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
    echo '<input type="hidden" name="_method" value="put">';
    echo '<input type="hidden" name="_method" value="delete">';
    echo '<input type="submit">';
    echo '</form>';
});

Route::post('form', function () {
    return 'post method';
});

//Route::put('form', function () {
//    return 'PUT method';
//});
Route::delete('form', function () {
    $r=route('home');
    echo '<a href="'.$r.'">HOME</a><br>';
    return 'DELETE method';
});

Route::view('/','welcome');

Route::get('/about', function () {
    return  view('about')->middleware('BeforeMiddleware');
});






//=================================================
//Route::get('/', function (Request $request) {
//$user =App\User::all();
////dd($request->$user);
//return $request->fullUrl();
//})->middleware('BeforeMiddleware');
Route::get('/',function (Request $request){

    echo "<form>";
    echo "<input type='text' name='name' value='".$request->old('name')."'>";
    echo "<input type='submit'>";
    echo "</form>";
    $name = $request->query('name');
    echo $request->old('name').'<br>';
    echo 'query '.$name;
//    dump($request);
    $uname=$request->old('name');
    echo $uname;
    $value = Cookie::get('name');
    echo $value;
    $minutes =0;
    return response('Hello World')->cookie(
        'name', 'value', $request->minutes,$request->path(),$request->domain
    );
//
//    if ($request->has(['name'])) {
//        echo 'name:'.$name.'<br>';
//    }
//    return $name;
    });
Route::get('homer', function () {
    return response('Hello World', 202)
        ->withHeaders(['Content-Type' => 'text/plain','Connection'=>'OPen'])->cookie('NAMER',10,60);
});
//$a=route('about',['id'=>]);
Route::get('/textfile', function () {
//    echo '<a href="/'.$a.'">HOME</a><br>';
    return  view('about');
});



//=============

Route::get('/child',function (){
    return view('child');
});
Route::get('/alert/{name}',function ($name){
    return view('alertsub',['name'=> $name]);
});
Route::get('greeting/{name}', function ($name) {
    return view('welcome', ['name' => $name]);
});
Route::get('/jsonfile',function (){

    return view('json');
});
//Route::get('/controls',function (){
//    $subject=['a','b','c','d'];
//    return view('json')->with('subject',$subject);
//});


//+++++++++++++
Route::get('/about/{id?}', function ($id) {

    return  '<h1>This is From above URL :'.$id.'</h1>';
})->where('id', '[1-9]+');

Route::get('/home', "PagesController@index" )->name('home');

Route::group(['prefix'=>'protected'],function (){
    Route::get('account',function (){
        echo 'Account!!';
    });
    Route::get('security',function (){
        echo 'Security!!';
    });
    Route::get('/',function (){

        echo "<form>";
        echo "<input type='text' name='name'>";
        echo "<input type='submit'>";
        echo "</form>";
        $name = $request->input('name');
        return $name;    });
});
//Route::get('user/add',function (){
//    return view('form');
//// echo App\User::all();
////    echo '<form action="/user" method="POST">';
////    echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
////    echo '<br>Name:<input type="name" name="name"  >';
////    echo '<br>Email:<input type="email" name="email" >';
////    echo '<br>Password:<input type="password" name="password"  >';
////    echo '<input type="submit">';
////    echo '</form>';
//});
//Route::get('userdisplay/{id}', function ($id) {
//    echo App\User::find($id);
//});
//Route::get('userdisplay', function () {
//    echo App\User::all();
//});
//Route::post('userdisplay',function (Request $request){
////    dd($request->all());
////    App\User::create($request->all());
////    echo App\User::all();
//
//   $user= new App\User();
//   $user->name=$request->name;
//   $user->email=$request->email;
//   $user->password = bcrypt($request->password);
//   $user->save();
////    echo App\User::all();
////   $user->name="aaa";
////   $user->email="a@a.com";
////   $user->password = bcrypt(123);
////   $user->save();
//});
//Route::get('user/adduser',function (){
////    return view('form');
//// echo App\User::all();
//    echo '<form action="/user" method="POST">';
//    echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
//    echo '<br>Name:<input type="name" name="name"  >';
//    echo '<br>Email:<input type="email" name="email" >';
//    echo '<br>Password:<input type="password" name="password"  >';
//    echo '<input type="submit">';
//    echo '</form>';
//});
//Route::get('user/{id}', function ($id) {
//    echo App\User::find($id);
//});
//Route::get('user', function () {
////    echo App\User::all();
//     echo App\User::wherein('name',['aaa','abc','ccc'])->get();
////     echo App\User::find(1);
////     echo App\User::find([1,3,4]);
//});
////Route::get('user', function () {
//////     echo App\User::find(1);
//////     echo App\User::find([1,3,4]);
////});
////Route::get('user', function () {
//////     echo App\User::all();
//////     echo App\User::all()->where('id','>',5);
//////     echo App\User::find(1);
//////     echo App\User::find([1,3,4]);
////});
//Route::post('user',function (Request $request){
////    dd($request->all());
//    App\User::create($request->all());
////    echo App\User::all();
////
////   $user= new App\User();
////   $user->name=$request->name;
////   $user->email=$request->email;
////   $user->password = bcrypt($request->password);
////   $user->save();
////    echo App\User::all();
////   $user->name="aaa";
////   $user->email="a@a.com";
////   $user->password = bcrypt(123);
////   $user->save();
//});
//Route::get('userdisplay1',function (Request $request){
//
//
//    $user1= App\User::find(7);
////    dd($user1);
//    $user1->name='seven';
//    $user1->save();
//    echo App\User::all();
////    echo App\User::all();
////   $user->name="aaa";
////   $user->email="a@a.com";
////   $user->password = bcrypt(123);
////   $user->save();
//});
//
//Route::get('/chunki',function(){
//
//    \App\User::chunk(2, function ($users) {
//        foreach ($users as $user) {
//            echo $user;
//            echo '<br>';
//        }
//    });
//});
Route::get('/crsor',function (){
    $users = App\User::cursor()->filter(function ($user) {
        return $user->id > 5;
    });

    foreach ($users as $user) {
        echo $user->id ."<br>";
    }
});
Route::get('/midphone','MiddlewareController@phonedisplay');
Route::get('/miduser','MiddlewareController@userdis');

Route::get('/middleware', 'MiddlewareController');
Route::resource('user', 'UserController');
Route::resource('video', 'VideoController');
Route::get('/many', 'ManyController@Manytomany');
Route::get('/category/{id}', 'ManyController@category');
Route::get('/product/{id}', 'ManyController@product');
Route::get('/collection',function(){
    //==
//    $user = App\User::find(1);
//    $user->name = 'aa';
//    $user->save();
//    echo $user->name;
//    //==
//    $user = App\User::find(1);
//
//    $user->deleted_at = now();
//
//    $user->save();
//    return $user->deleted_at;
//    ==
//    $collectionA=collect([1,2,3]);
//    $collectionB=$collectionA->collect();
//    return $collectionB->all();
//    =======
//    $collection=collect(['Jhon','Doe']);
//    $con=$collection->concat(['Jhon Doe'])->concat(['name'=>'Jhon Doe']);
//    echo $collection->contains('Doe');
//    return $con->all();
//    return $con->count();

//    $collection = collect([1, 2, 2, 2, 3]);
//    $counted=$collection->countBy();
//    return $counted->all();

//$col=collect(['abc@mail.com','bcd@bail.com','cde@mail.com']);
////    $col = collect(['alice@gmail.com', 'bob@yahoo.com', 'carlos@gmail.com']);
//    $counted=$col->countBy(function ($email){
//        return substr(strchr($email,'@'),1);
//    });
//    return $counted->all();

//    $collection = collect([
//        'color' => 'orange',
//        'type' => 'fruit',
//        'remain' => 6
//    ]);
//    $diff=$collection->diffKeys([
////    $diff=$collection->diffAssoc([
//        'color' => 'yellow',
//        'type' => 'fruit',
//        'remain1' => 3,
//        'used' => 6,
//    ]);
//    return $diff->all();
//    return $collection->dump();
//    $employees = collect([
//        ['email' => 'a@e.com', 'position' => 'Developer'],
//        ['email' => 'b@e.com', 'position' => 'Designer'],
//        ['email' => 'b@e.com', 'position' => 'Developer'],
//    ]);
//    return $employees->duplicates('position');
//    $collection = collect([['John Doe', 35], ['Jane Doe', 33]]);
//
//    $collection->eachSpread(function ($name, $age) {
//        echo "$name - $age <br>";
//    });
//    $collection = collect([1, 2, 3, 4]);//->every(function ($value, $key) {
//        $collection->every(function ($value, $key) {
//            return $value > 2;
//        });
//    $collection = collect([1, 2, 3, 4]);
//
//    $filtered = $collection->filter(function ($value, $key) {
//        return $value > 2;
//    });
//
//    return $filtered->all();
//    $collection = collect([['name'=>'abc'],['addr'=>'addr']]);
//    $fltr=$collection->flatMap(function ($values){
//        return array_map('strtoupper',$values);
//    });
//    return $fltr->all();
//    $collection = collect(['name' => 'taylor', 'languages' => ['php', 'javascript']]);
//
//    $flattened = $collection->flatten();
//
//    return $flattened->all();
//    $collection=collect(['a'=>1,'b'=>2]);
//    $flipped=$collection->flip();
//    return $flipped->all();
//    $collection = collect([1, 2, 3, 4, 5, 6, 7, 8, 9]);
//    $chunk = $collection->forPage(2, 3);
//    return $chunk->all();

//    $collection = collect([1, 2, 3, 4, 5]);
//
//    return $collection->implode('+');
//   return collect(['a', 'b', 'c'])->join('');
//    $collection = collect([
//        ['product_id' => 'prod-100', 'name' => 'Desk'],
//        ['product_id' => 'prod-100', 'name' => 'Chair'],
//    ]);
//
//    $keyed = $collection->keyBy('product_id');
//
//    return $keyed->all();
}) ;
Route::get('/about', function () {
    return view('about');
})->Middleware(['CheckAge','CheckIp']);
Route::get('/hello/{age}', function ($age) {
    session(['age' => $age]);
    return "Its Hello";
})->Middleware(['CheckAge']);
//
//
/*
/home/wmt/snap/postman/97/Postman/files
*/
