<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('album_id')->unsigned();
            $table->String('title');
            $table->String('file');
            $table->enum('track_type',['pop','classisc','jazz','rock','disco']);
            $table->time('track_time');
            $table->timestamps();
            $table->foreign('album_id')->references('id')->on('album');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track');
    }
}
