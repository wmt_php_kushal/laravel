<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::View('/update','updatepic');
Route::get('user/{id}','SocialController@user');

Route::get('alluser','SocialController@allusers');

Route::get('update/{id}','SocialController@update');

Route::get('post/{id}','SocialController@post');
Route::get('posters/{id}','SocialController@posters');

Route::get('comments/{id}','SocialController@comments');

Route::get('comment/{id}/video','SocialController@commentbyvideo');

Route::get('comment/{id}/image','SocialController@commentbyimage');

Route::get('comment/image','SocialController@commentbyimageall');

Route::get('comment/video','SocialController@commentbyvideoall');

Route::get('post/all/images','SocialController@postbyimageall');

Route::get('post/all/videos','SocialController@postbyvideoall');

Route::get('post/image/{id}','SocialController@postbyimage');

Route::get('post/video/{id}','SocialController@postbyvideo');

Route::get('country/{id}/posts','SocialController@countryPosts');

Route::get('country/{id}/users','SocialController@countryUser');

Route::get('country/{id}/images','SocialController@countryImage');

