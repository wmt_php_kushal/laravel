<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable=[
        'title','body','user_id'
    ];
    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }
    public function  users(){
        return $this->belongsToMany(Post::class);
    }
    public function postable()
    {
        return $this->morphTo();
    }
}
