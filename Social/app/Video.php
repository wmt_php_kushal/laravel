<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    public function comments()
    {
//dd('111');
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function posts()
    {
//dd('111');
        return $this->morphMany(Post::class, 'postable');
    }

}
