<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function comments()
    {
//        dd('122');
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function posts()
    {
//        dd('122');
        return $this->morphMany(Post::class, 'postable');
    }
}
