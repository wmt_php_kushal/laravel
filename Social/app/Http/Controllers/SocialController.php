<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Country;
use App\Image;
use App\Post;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection ;

class SocialController extends Controller
{
    //
    public function post($id){
        return Post::with(['comments'])->find($id);
    }
    public function comments($id){
        return Comment::with(['commentable'])->findOrFail($id);
    }
    public function user($id){
//        return User::with('posts')->find($id);
        return User::with(['posts','comments'])->find($id);
    }
    public function posters($id){
        return User::with('posts')->find($id);
//        return User::with(['posts','comments'])->find($id);
    }
    public function countryPosts($id){
        return Country::with('posts','users')->findOrFail($id);
    }
    public function countryUser($id){
        return Country::with('users')->findOrFail($id);
    }
    public function countryImage($id){
        return Country::with('country_image')->findOrFail($id);
    }
    public function commentbyvideo($id){
        $vid = Video::find($id);
        foreach ($vid->comments as $comment) {
            echo $comment;
        }
    }
    public function commentbyimage($id){
//        $vid = Image::find($id);
        return Image::find($id)->comments;
//        foreach ($vid->comments as $comment) {
//            return $comment .'<br>';
//        }
    }
    public function commentbyimageall(){
       return Image::with('comments')->get();
    }
    public function commentbyvideoall(){
       return Video::with('comments')->get();
    }
    public function postbyimage($id){
        return Image::find($id)->posts;
    }
    public function postbyvideo($id){
        return Video::find($id)->posts ;
    }
    public function postbyimageall(){
//        dd('111111');
        return Image::with('posts','users')->get();
    }
    public function postbyvideoall(){

        return Video::with('posts')->get();

    }
    public function allusers(){
        return User::all();
    }
    public function update($id)
    {
            $user= User::find(1);
            $user->profile_pic="new pic";
            $user->save();
            return "Value Updated";
    }
}
