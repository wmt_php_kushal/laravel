<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable=[
        'name'
    ];
    public function posts(){
//        return $this->hasOneThrough(Post::class,User::class);
        return $this->hasManyThrough(Post::class,User::class);
//        country wise single image vada trial
        return $this->hasManyThrough(Post::class,User::class)->where('id','=',$id);
    }
    public function users(){
//        return $this->hasOneThrough(Post::class,User::class);
        return $this->hasMany(User::class);

    }
    public function country_image(){
        return $this->hasOneThrough(Image::class,Post::class,'user_id=1');


    }
}
