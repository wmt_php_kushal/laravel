<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'id','name', 'email', 'password', 'country_id','profile_pic'
    ];
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function comments(){
        return $this->morphMany('App\Comment', 'commentable');
    }
}
