<?php

namespace App\Http\Controllers;

use App\Album;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class AblumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movie=Album::all();
        return view('displaymovie',['movie'=> $movie]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movieinsertform',['movie'=>Album::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'bail|required|max:255',
            'coverimage' => 'required',
            'published_date' => 'required|date|after:3/25/2020',
            'gender'=>'required',
            'phone'=>'numeric|digits:10'
        ]);

        Album::create($request->all());
//        $validated = $request->validated();
        return redirect()->route('movie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        Album::create($request->all());
        return redirect()->route('movie.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Album $a)
    {
        return view('movieupdateform',['movie'=>$a]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Album $a, $id)
    {

        $a->update($request->all());
        return redirect()->route('movie.index');

        $a->update($request->all());
        return redirect()->route('movie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aa=Album::find($id);
        $aa->delete();
        return redirect()->route('movie.index');
    }
}
