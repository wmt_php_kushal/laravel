
@extends('home')
@section('content')
    <div class="container">
        <td>
            <form action="/movie/create">
                <input type="submit" value="Create"></form></td>
        <table border=solid 1px >
            <tr>
                <th>id</th>
                <th>Title</th>
                <th>CoverImage</th>
                <th>Published Date</th>

                <th>Update</th>
                <th>delete</th>
            </tr>
            @foreach($movie as $u)
                <tr>
                    {{--         {{$u->id}}--}}
                    <td>{{$u->id}}</td>
                    <td>{{$u->title}}</td>
                    <td>{{$u->coverimage}}</td>
                    <td>{{$u->published_date}}</td>
                    <td>
                        <form action="/movie/{{$u->id}}/edit">
                            <input type="submit" value="Update">
                        </form>
                    </td>
                    <td>
                        <form action="movie/{{$u->id}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" >
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Delete">
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
