@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                        <p><a href="display/">Display</a></p>
                        <p><a href="movie/moviedisplay/">Show Movie</a></p>
                        <p><a href="insertform/">Insert</a></p>
                        <p><a href="movieinsert/">Movie Insert</a></p>
                        @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
