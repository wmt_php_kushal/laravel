@extends('home')
@section('content')
    <form action="{{route('movie.store')}}" method="POST">
        <input type="hidden" name="_token"  value="{{csrf_token()}}">
        <div><br>Title:<input type="text" name="title">@error('title')
            <span class="alert text-alert">{{ $message }}</span>
            @enderror</div>

        <div><br>CoverImage:<input type="text" name="coverimage">@error('coverimage')
            <span class="alert text-alert">{{ $message }}</span>
            @enderror</div>
        <div>
            <br>Released Date:<input type="date" class="@error('published_data')is-invalid @enderror" name="published_date">
            @error('published_date')
            <span class="alert text-alert">{{ $message }}</span>
            @enderror
        </div>
        <div>
            <br>Number:<input type="tel" class="@error('phone')is-invalid @enderror" name="phone">
            @error('phone')
            <span class="alert text-alert">{{ $message }}</span>
            @enderror
        </div>
        <div>
            <br>GENDER:<br><input type="radio" name="gender" class="@error('gender')is-invalid @enderror" value="male"> Male<br>
            <input type="radio" name="gender" class="@error('gender')is-invalid @enderror" value="female"> Female<br>
{{--            <input type="RADIO" class="@error('gender')is-invalid @enderror" name="gender">--}}
            @error('gender')
            <span class="alert text-alert">{{ $message }}</span>
            @enderror
        </div>
        <br><input type="submit">
    </form>
{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            <ul>--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <li>{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    @endif--}}

@endsection
