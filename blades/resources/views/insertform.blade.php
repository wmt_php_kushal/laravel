@extends('home')
@section('content')
    <form action="/movie/{{$movie->id}}" method="POST">
        <input type="hidden" name="_token" value="csrf_token()" >
        <div><br>Title:<input type="text" name="title"></div>
        <div><br>CoverImage:<input type="text" name="coverimage"></div>
        <div><br>Released Date:<input type="date" name="published_date"></div>
        <br><input type="submit">
    </form>
@endsection
